import { OAuthService } from 'angular-oauth2-oidc';
import { JwksValidationHandler } from 'angular-oauth2-oidc';
import { authConfig } from './auth.config';
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Headers, RequestOptions } from '@angular/http';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private oauthService: OAuthService, private http: HttpClient) {
    this.oauthService.configure(authConfig);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.tryLogin();
    if (!this.oauthService.hasValidAccessToken()) {
      this.oauthService.initImplicitFlow();
    } else {
      var /** @type {?} */ params = new HttpParams()
        .set('grant_type', 'urn:ietf:params:oauth:grant-type:jwt-bearer')
        .set('client_id', 'https://fox-contact.markarian-ota.nl/')
        .set('client_secret', 'inGLgX4UF78qBd7b5niskrQmZfwSFbz5pi2PCbhT')
        .set('scope', 'openid')
        .set('requested_token_use', 'on_behalf_of')
        .set('resource', 'https://saxton-o.markarian-ota.nl/')
        .set('assertion', this.oauthService.getAccessToken());
      var /** @type {?} */ headers = new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded');
      http.post(authConfig.tokenEndpoint, params, { headers: headers, responseType: 'text' }).subscribe(function (response) {
        console.debug('obo response', response);
      }, function (err) {
        console.error('Error', err);
      });
    }
  }

  get id_token() {
    return this.oauthService.getIdToken();
  }

  get access_token() {
    return this.oauthService.getAccessToken();
  }
}
